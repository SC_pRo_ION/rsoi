package postclasses;

public class CheckTokenResponse {

	private boolean valid;
	
	private long userId = -1;
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public boolean getValid() {
		return valid;
	}
	
	public long getUserId() {
		return userId;
	}
}
