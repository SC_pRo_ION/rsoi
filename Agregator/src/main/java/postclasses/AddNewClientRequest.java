package postclasses;

public class AddNewClientRequest {

    private String company;
    
    private String redirectUri;
    
    public AddNewClientRequest() {
    	
    }
    
    public String getCompany() {
    	return company;
    }
    
    public String getRedirectUri() {
    	return redirectUri;
    }
    
    public void setCompany(String company) {
    	this.company = company;
    }
    
    public void setRedirectUri(String redirectUri) {
    	this.redirectUri = redirectUri;
    }
}
