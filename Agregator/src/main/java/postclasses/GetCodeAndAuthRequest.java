package postclasses;

public class GetCodeAndAuthRequest {

	private String clientId;
	
	private String login;
	
	private String password;
	
	public GetCodeAndAuthRequest() {
		
	}

	public String getClientId() {
		return clientId;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
