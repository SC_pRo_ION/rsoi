package postclasses;

public class AllowAuthResponse {
	
	private boolean ok = true;
	
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	
	public boolean getOk() {
		return ok;
	}
}
