package postclasses;

import java.util.List;

import model.Message;

public class MessageListResponse {

	private List <Message> messages;
	
	private long page;
	
	private long size;
	
	private long pageCount;
	
	public MessageListResponse() {
		
	}
	
	public MessageListResponse(long size, long page, int pageCount) {
		this.size = size;
		this.page = page;
		this.pageCount = pageCount;
	}
	
	public MessageListResponse(long size, long page, long pageCount, List <Message> messages) {
		this.size = size;
		this.page = page;
		this.pageCount = pageCount;
		this.messages = messages;
	}
	
	public List <Message> getMessages() {
		return messages;
	}
	
	public void setMessages(List <Message> messages) {
		this.messages = messages;
	}
	
	public long getPage() {
		return page;
	}
	
	public void setPage(long page) {
		this.page = page;
	}
	
	public long getPageCount() {
		return pageCount;
	}
	
	public void setPageCount(long pageCount) {
		this.pageCount = pageCount;
	}
	
	public long getSize() {
		return size;
	}
	
	public void setSize(long size) {
		this.size = size;
	}
}