package postclasses;

public class AddUserRequest {

	private String login;
	private String name;
	private String sirname;
	private String password;
    
    public AddUserRequest() {
    	
    }
    
    public String getLogin() {
    	return login;
    }
    
    public String getSirname() {
    	return sirname;
    }
    
    public String getName() {
    	return name;
    }
    
    public String getPassword() {
    	return password;
    }
    
    public void setLogin(String login) {
    	this.login = login;
    }
    
    public void setSirname(String sirname) {
    	this.sirname = sirname;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setPassword(String password) {
    	this.password = password;
    }
}
