package postclasses;

public class AddNewClientResponse {

	private String clientId;
	
	private String clientSecret;
	
	private int code;
	
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	
	public String getClientSecret() {
		return this.clientSecret;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public String getClientId() {
		return this.clientId;
	}
	
	public void setCode(boolean ok) {
		code = 200;
	}
	
	public int getCode() {
		return this.code;
	}
}
