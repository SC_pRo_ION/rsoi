package postclasses;

public class GetCodeRequest {

	private String clientId;
	
	private long userId;
	
	public GetCodeRequest(String clientId, long userId) {
		this.clientId = clientId;
		this.userId = userId;
	}
	
	public String getClientId() {
		return clientId;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
}
