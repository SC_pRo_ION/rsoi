package postclasses;

public class CheckTokenRequest {

	private String token;
	
	public CheckTokenRequest() {
		
	}
	
	public CheckTokenRequest(String token) {
		this.token = token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}
}
