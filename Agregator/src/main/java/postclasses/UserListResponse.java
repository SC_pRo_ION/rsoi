package postclasses;

import java.util.List;

import model.UserModel;

public class UserListResponse {

	private List <UserModel> users;
	
	private long page;
	
	private long size;
	
	private long pageCount;
	
	public UserListResponse() {
		
	}
	
	public UserListResponse(long size, long page, int pageCount) {
		this.size = size;
		this.page = page;
		this.pageCount = pageCount;
	}
	
	public UserListResponse(long size, long page, long pageCount, List <UserModel> users) {
		this.size = size;
		this.page = page;
		this.pageCount = pageCount;
		this.users = users;
	}
	
	public List <UserModel> getUsers() {
		return users;
	}
	
	public void setUsers(List <UserModel> users) {
		this.users = users;
	}
	
	public long getPage() {
		return page;
	}
	
	public void setPage(long page) {
		this.page = page;
	}
	
	public long getPageCount() {
		return pageCount;
	}
	
	public void setPageCount(long pageCount) {
		this.pageCount = pageCount;
	}
	
	public long getSize() {
		return size;
	}
	
	public void setSize(long size) {
		this.size = size;
	}
}
