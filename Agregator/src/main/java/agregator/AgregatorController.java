package agregator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import model.*;
import postclasses.*;

@Controller
public class AgregatorController {

	private String OAUTH_LINK = "http://localhost:8083/";	
	private String MESSAGE_LINK = "http://localhost:8082/";
	private String USER_LINK = "http://localhost:8081/";
	
    @Autowired
    HttpSender httpSender;
    
    @RequestMapping(value = "/addClient", method = RequestMethod.POST)
    @ResponseBody
    public AddNewClientResponse addClient(@ModelAttribute AddNewClientRequest params) {
    	return httpSender.addClient(OAUTH_LINK + "addClient", params);
    }
    
    @RequestMapping("/authorization")
    @ResponseBody
    public ModelAndView getOAuth(
            @RequestParam(value = "redirect_uri", required = true) String redirectUri,
            @RequestHeader(value = "Authorization") String clientId) {
    	clientId = clientId.substring(clientId.indexOf("Basic ") + 6);
    	AllowAuthResponse res = httpSender.askAuthorization(OAUTH_LINK + "authorization?client_id=" + clientId + "&redirect_uri=" + redirectUri);
    	ModelAndView mav = new ModelAndView("auth");
    	mav.addObject("clientId", clientId);
    	return mav; 
    }

    @RequestMapping(value = "/getCode", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView getCode(@ModelAttribute GetCodeAndAuthRequest params) throws IllegalAccessException {
    	GetUserIdRequest authrequest = new GetUserIdRequest(params.getLogin(), params.getPassword());
    	GetUserIdResponse authresp = httpSender.getUserId(USER_LINK + "/userId", authrequest);
    	if (authresp.getId() == -1) {
            throw new IllegalAccessException("Invalid login/password");
        }
    	GetCodeRequest coderequest = new GetCodeRequest(params.getClientId(), authresp.getId());
    	GetCodeResponse res = httpSender.getCode(OAUTH_LINK + "getCode", coderequest);
    	
    	String redirect = "redirect:" + res.getRedirectUri() + "?code=" + res.getCode();
    	return new ModelAndView(redirect);
    }

    @RequestMapping("/accessToken")
    @ResponseBody
    public OAuthModel getAccessToken(
            @RequestParam(value = "redirect_uri", required = true) String redirectUri,
    		@RequestParam(value = "code", required = true) String code,
            @RequestHeader(value = "Authorization") String ids) {
    	String clientId = ids.substring(ids.indexOf("Basic ") + 6, ids.indexOf(':'));
    	String clientSecret = ids.substring(ids.indexOf(':') + 1);
    	return httpSender.getAccessToken(OAUTH_LINK + "accessToken?client_id=" + clientId + "&client_secret=" + 
    		clientSecret + "&redirect_uri=" + redirectUri + "&code=" + code);
    }
    
    @RequestMapping(value = "/userId", method = RequestMethod.POST)
    @ResponseBody
    public GetUserIdResponse getUserId(@ModelAttribute GetUserIdRequest params) {
    	return httpSender.getUserId(USER_LINK + "/userId", params);
    }
    
    @RequestMapping("/user/{userId}")
    @ResponseBody
    public UserModel getUser(@PathVariable("userId") long userId,
            @RequestHeader(value = "Authorization") String token) throws IllegalAccessException {

        CheckTokenRequest checkTokenRequest = new CheckTokenRequest(token);
        CheckTokenResponse checkTokenResponse = httpSender.checkToken(OAUTH_LINK + "checkToken", checkTokenRequest);
        if (checkTokenResponse == null || !checkTokenResponse.getValid()) {
            throw new IllegalAccessException("Invalid token");
        }
        
        return httpSender.getUser(USER_LINK + "/user/" + userId);
    }
    
    @RequestMapping("/me")
    @ResponseBody
    public UserModel getUser(@RequestHeader(value = "Authorization") String token) throws IllegalAccessException {

        CheckTokenRequest checkTokenRequest = new CheckTokenRequest(token);
        CheckTokenResponse checkTokenResponse = httpSender.checkToken(OAUTH_LINK + "checkToken", checkTokenRequest);
        if (checkTokenResponse == null || !checkTokenResponse.getValid()) {
            throw new IllegalAccessException("Invalid token");
        }
        
        return httpSender.getUser(USER_LINK + "/user/" + checkTokenResponse.getUserId());
    }
    
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public UserListResponse getUsers(
    		@RequestParam(value = "page", required=false) Long page,
            @RequestParam(value = "size", required=false) Long size) throws IllegalAccessException {
        if (page == null) page = 1L;
        if (size == null) size = 10L;
        UserListResponse users = httpSender.getUsers(USER_LINK + "users?page=" + page + "&size=" + size);

        return users;
    }
    
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ResponseBody
    public AddUserResponse addUser(@ModelAttribute AddUserRequest params, 
    		@RequestHeader(value = "Authorization") String token) throws IllegalAccessException {
        CheckTokenRequest checkTokenRequest = new CheckTokenRequest(token);
        CheckTokenResponse checkTokenResponse = httpSender.checkToken(OAUTH_LINK + "checkToken", checkTokenRequest);
        if (checkTokenResponse == null || !checkTokenResponse.getValid()) {
            throw new IllegalAccessException("Invalid token");
        }
    	return httpSender.addUsers(USER_LINK + "addUser", params);
    }
    
    @RequestMapping("/getMessage/{id}")
    @ResponseBody
    public Message message(@PathVariable("id") long id, 
    		@RequestHeader(value = "Authorization") String token) throws IllegalAccessException {
        CheckTokenRequest checkTokenRequest = new CheckTokenRequest(token);
        CheckTokenResponse checkTokenResponse = httpSender.checkToken(OAUTH_LINK + "checkToken", checkTokenRequest);
        if (checkTokenResponse == null || !checkTokenResponse.getValid()) {
            throw new IllegalAccessException("Invalid token");
        }
    	return httpSender.getMessage(MESSAGE_LINK + "getMessage/" + id + '/');
    }
    
    @RequestMapping("/getMessages/{ownerId}")
    @ResponseBody
    public MessageListResponse messages(@PathVariable("ownerId") long ownerId,
    		@RequestParam(value = "page", required=false) Long page,
            @RequestParam(value = "size", required=false) Long size, 
    		@RequestHeader(value = "Authorization") String token) throws IllegalAccessException {
        CheckTokenRequest checkTokenRequest = new CheckTokenRequest(token);
        CheckTokenResponse checkTokenResponse = httpSender.checkToken(OAUTH_LINK + "checkToken", checkTokenRequest);
        if (checkTokenResponse == null || !checkTokenResponse.getValid()) {
            throw new IllegalAccessException("Invalid token");
        }
        if (page == null) page = 1L;
        if (size == null) size = 10L;
        return httpSender.getMessages(MESSAGE_LINK + "getMessages/" + ownerId + "?page=" + page + "&size=" + size);
    }
    
    @RequestMapping(value = "/postMessage", method = RequestMethod.POST)
    @ResponseBody
    public PostMessageResponse postMessage(@ModelAttribute PostMessageRequest params, 
    		@RequestHeader(value = "Authorization") String token) throws IllegalAccessException {
        CheckTokenRequest checkTokenRequest = new CheckTokenRequest(token);
        CheckTokenResponse checkTokenResponse = httpSender.checkToken(OAUTH_LINK + "checkToken", checkTokenRequest);
        if (checkTokenResponse == null || !checkTokenResponse.getValid()) {
            throw new IllegalAccessException("Invalid token");
        }
    	return httpSender.postMessages(MESSAGE_LINK + "postMessage", params);
    }
}
