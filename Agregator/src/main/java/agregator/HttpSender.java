package agregator;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import postclasses.*;
import model.*;

@Service
public class HttpSender {

    private static final RestTemplate restTemplate = new RestTemplate();

    public CheckTokenResponse checkToken(String url,  CheckTokenRequest params) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("token", params.getToken());
        return restTemplate.postForObject(url, map, CheckTokenResponse.class);
    }

    public UserListResponse getUsers(String url){
        return restTemplate.getForObject(url, UserListResponse.class);
    }

	public AddNewClientResponse addClient(String url, AddNewClientRequest params) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("company", params.getCompany());
		map.add("redirectUri", params.getRedirectUri());
		return restTemplate.postForObject(url, map, AddNewClientResponse.class);
	}

	public AllowAuthResponse askAuthorization(String url) {
        return restTemplate.getForObject(url, AllowAuthResponse.class);
	}

	public GetUserIdResponse getUserId(String url, GetUserIdRequest params) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("login", params.getLogin());
		map.add("password", params.getPassword());
		return restTemplate.postForObject(url, map, GetUserIdResponse.class);
	}

	public UserModel getUser(String url) {
        return restTemplate.getForObject(url, UserModel.class);
	}

	public AddUserResponse addUsers(String url, AddUserRequest params) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("login", params.getLogin());
		map.add("password", params.getPassword());
		map.add("name", params.getName());
		map.add("sirname", params.getSirname());
		return restTemplate.postForObject(url, map, AddUserResponse.class);
	}

	public GetCodeResponse getCode(String url, GetCodeRequest params) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("clientId", params.getClientId());
		map.add("userId", Long.toString(params.getUserId()));
		return restTemplate.postForObject(url, map, GetCodeResponse.class);
	}

	public OAuthModel getAccessToken(String url) {
        return restTemplate.getForObject(url, OAuthModel.class);
	}

	public Message getMessage(String url) {
        return restTemplate.getForObject(url, Message.class);
	}

	public MessageListResponse getMessages(String url) {
        return restTemplate.getForObject(url, MessageListResponse.class);
	}

	public PostMessageResponse postMessages(String url, PostMessageRequest params) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("message", params.getMessage());
		map.add("ownerId", Long.toString(params.getOwnerId()));
		return restTemplate.postForObject(url, map, PostMessageResponse.class);
	}
}