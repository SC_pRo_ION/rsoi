package model;

import lombok.Data;

@Data
public class UserModel {
    private long id;
    
    private String login;
    
    private String name;
    
    private String sirname;
    
    private String password;
    
    public UserModel() {
    	
    }

    public long setId() {
    	return id;
    }
    
    public long getId() {
    	return id;
    }
    
    public String getLogin() {
    	return login;
    }
    
    public String getSirname() {
    	return sirname;
    }
    
    public String getName() {
    	return name;
    }
    
    public String getPassword() {
    	return password;
    }
    
    public void setLogin(String login) {
    	this.login = login;
    }
    
    public void setSirname(String sirname) {
    	this.sirname = sirname;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setPassword(String password) {
    	this.password = password;
    }
}