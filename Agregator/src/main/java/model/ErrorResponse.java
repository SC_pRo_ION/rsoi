package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

	@JsonProperty("message")
    private String message;

	public ErrorResponse() {
		
	}
	
    public ErrorResponse(String message) {
        this.message = message;
    }
    
    public String getMessage() {
    	return message;
    }
    
    public void setMessage(String message) {
    	this.message = message;
    }
}