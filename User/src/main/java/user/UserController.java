package user;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.UserModel;
import postclasses.*;
import repository.UserRepository;

@RestController
public class UserController {

	@Autowired
	UserRepository userRepository;

    @RequestMapping(value = "/userId", method = RequestMethod.POST)
    @ResponseBody
    public GetUserIdResponse getUserId(@ModelAttribute GetUserIdRequest params) {
    	GetUserIdResponse res = new GetUserIdResponse();
    	UserModel user = userRepository.findByLoginAndPassword(params.getLogin(), params.getPassword());
    	res.setId(user == null ? -1 : user.getId());
    	res.setCode(true);
    	return res;
    }
    
    @RequestMapping("/user/{userId}")
    @ResponseBody
    public UserModel getUser(@PathVariable("userId") long userId) {
        return userRepository.findOne(userId);
    }
    
    @RequestMapping("/users")
    @ResponseBody
    public UserListResponse users(
    		@RequestParam(value = "page", required=false) Long page,
            @RequestParam(value = "size", required=false) Long size) {
        if (page == null) page = 1L;
        if (size == null) size = 10L;
        List <UserModel> users = (List<UserModel>) userRepository.findAll();
        Long from = page * size - size;
        long pageCount = users.size() / size + (users.size() % size > 0 ? 1 : 0);
        users =
        		users.stream()
                        .skip(from)
                        .limit(size)
                        .collect(Collectors.toList());
        return new UserListResponse(size, page, pageCount, users);
    }
    
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ResponseBody
    public AddUserResponse addUser(@ModelAttribute AddUserRequest params) {
    	AddUserResponse res = new AddUserResponse();
    	UserModel user = new UserModel();
    	user.setLogin(params.getLogin());
    	user.setName(params.getName());
    	user.setSirname(params.getSirname());
    	user.setPassword(params.getPassword());
    	userRepository.save(user);
    	
    	res.setId(user.getId());
    	res.setCode(true);
    	return res;
    }
}
