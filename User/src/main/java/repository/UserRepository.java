package repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import model.UserModel;

@Component
public interface UserRepository extends CrudRepository<UserModel,Long>{

	public UserModel findByLoginAndPassword(String login, String password);
}
