package postclasses;

public class GetCodeResponse {

	private String redirectUri;
	
	private String code;
	
	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getRedirectUri() {
		return redirectUri;
	}
	
	public String getCode() {
		return code;
	}
}
