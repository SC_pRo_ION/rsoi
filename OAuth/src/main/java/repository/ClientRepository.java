package repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import model.Client;

@Component
public interface ClientRepository extends CrudRepository<Client,Long>{

	public List <Client> findClientsByClientIdAndRedirectUri(String clientId, String redirectUri);
	
	public Client findClientByClientId(String clientId);
}
