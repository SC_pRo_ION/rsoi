package repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import model.OAuthModel;

@Component
public interface OAuthRepository extends CrudRepository<OAuthModel,Long>{
	
	public OAuthModel findByCode(String code);
	
	public OAuthModel findByAccessToken(String accessToken);
}
