package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OAuthModel {

    @Id
	private long userId;
	
	private String code;
	
	private String accessToken;
	
	private String refreshToken;
	
	private String clientId;
	
	private int expires = 30000;
	
	public OAuthModel() {
		
	}
	
	public long getUserId() {
		return userId;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getAccessToken() {
		return accessToken;
	}
	
	public String getRefreshToken() {
		return refreshToken;
	}
	
	public String getClientId() {
		return clientId;
	}
	
	public int getExpires() {
		return expires;
	}

	public  void setUserId(long userId) {
		this.userId = userId;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public void setExpires(int expires) {
		this.expires = expires;
	}
}
