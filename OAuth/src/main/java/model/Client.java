package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Client {
	@Id
    private String clientId;

    private String clientSecret;
    
    private String company;
    
    private String redirectUri;
    
    public Client() {
    	
    }
    
    public String getClientId() {
    	return clientId;
    }
    
    public String getClientSecret() {
    	return clientSecret;
    }
    
    public String getCompany() {
    	return company;
    }
    
    public String getRedirectUri() {
    	return redirectUri;
    }
    
    public void setClientId(String clientId) {
    	this.clientId = clientId;
    }
    
    public void setClientSecret(String clientSecret) {
    	this.clientSecret = clientSecret;
    }
    
    public void setCompany(String company) {
    	this.company = company;
    }
    
    public void setRedirectUri(String redirectUri) {
    	this.redirectUri = redirectUri;
    }
}
