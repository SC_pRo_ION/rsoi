package oauth;

import java.util.List;
import java.util.Random;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import model.AllowAuthResponse;
import model.Client;
import model.OAuthModel;
import postclasses.*;
import repository.ClientRepository;
import repository.OAuthRepository;

@RestController
public class OAuthController {

	@Autowired
	OAuthRepository oauthRepository;

	@Autowired
	ClientRepository clientRepository;

    @RequestMapping(value = "/addClient", method = RequestMethod.POST)
    @ResponseBody
    public AddNewClientResponse addClient(@ModelAttribute AddNewClientRequest params) {
    	AddNewClientResponse res = new AddNewClientResponse();
    	Client client = new Client();
    	client.setClientId(generateRandomString(10, 15));
    	client.setClientSecret(generateRandomString(15, 20));
    	client.setCompany(params.getCompany());
    	client.setRedirectUri(params.getRedirectUri());
    	clientRepository.save(client);
    	
    	res.setClientId(client.getClientId());
    	res.setClientSecret(client.getClientSecret());
    	res.setCode(true);
    	return res;
    }
    
    @RequestMapping(value = "/getCode", method = RequestMethod.POST)
    @ResponseBody
    public GetCodeResponse getCode(@ModelAttribute GetCodeRequest params) {
    	Client client = clientRepository.findClientByClientId(params.getClientId());
    	if (client == null)
    		throw new EntityNotFoundException("No client with id " + params.getClientId() + " found.");
    	OAuthModel oauth = new OAuthModel();
    	oauth.setClientId(params.getClientId());
    	oauth.setUserId(params.getUserId());
    	oauth.setCode(generateRandomString(20, 22));
    	oauth.setAccessToken(generateRandomString(18, 20));
    	oauth.setRefreshToken(generateRandomString(18, 20));
    	OAuthModel existing = oauthRepository.findOne(params.getUserId());
    	if (existing != null)
    		oauthRepository.delete(params.getUserId());
    	oauthRepository.save(oauth);
    	GetCodeResponse res = new GetCodeResponse();
    	res.setCode(oauth.getCode());
    	res.setRedirectUri(client.getRedirectUri());
    	return res;
    }

    @RequestMapping(value = "/checkToken", method = RequestMethod.POST)
    @ResponseBody
    public CheckTokenResponse checkToken(@ModelAttribute CheckTokenRequest params) {
        if (params == null || params.getToken() == null || params.getToken().isEmpty())
            throw new IllegalArgumentException("No token sent.");
    	CheckTokenResponse res = new CheckTokenResponse();        
    	String token = params.getToken();
        if (!token.contains("Bearer")) {
        	res.setValid(false);
            return res;
        }
        String tokenValue = token.replace("Bearer ", "");
        OAuthModel tokenModel = oauthRepository.findByAccessToken(tokenValue);

        if (tokenModel == null) {
            res.setValid(false);
            return res;
        }
        res.setValid(true);
        res.setUserId(tokenModel.getUserId());
        return res;
    }
    
    @RequestMapping("/accessToken")
    @ResponseBody
    public OAuthModel getCode(
    		@RequestParam(value = "client_id", required = true) String clientId,
    		@RequestParam(value = "client_secret", required = true) String clientSecret,
            @RequestParam(value = "redirect_uri", required = true) String redirectUri,
    		@RequestParam(value = "code", required = true) String code) {
    	Client client = clientRepository.findClientByClientId(clientId);
    	if (client == null)
    		throw new EntityNotFoundException("No client with id " + clientId + " found.");
    	if (!client.getClientSecret().equals(clientSecret))
    		throw new EntityNotFoundException("No client with secret " + clientSecret + " found.");
    	if (!client.getRedirectUri().equals(redirectUri))
    		throw new EntityNotFoundException("No client with given parameters found.");
    	OAuthModel oauth = oauthRepository.findByCode(code);
    	if (oauth == null)
    		throw new EntityNotFoundException("Incorrect code.");
    	return oauth;
    }
    
    @RequestMapping("/authorization")
    @ResponseBody
    public AllowAuthResponse getOAuth(            
    		@RequestParam(value = "client_id", required = true) String clientId,
            @RequestParam(value = "redirect_uri", required = true) String redirectUri) {

    	List <Client> clients = clientRepository.findClientsByClientIdAndRedirectUri(clientId, redirectUri);
    	if (clients.isEmpty())
    		throw new EntityNotFoundException("Incorrect pair of client_id and redirect_uri");
    	
    	return new AllowAuthResponse();
    }
    
    private String generateRandomString(int lengthMin, int lengthMax) {
        String res = "";
        Random r = new Random();
        int length = r.nextInt(lengthMax - lengthMin) + lengthMin;
        for (int i = 0; i < length; ++i)
            res += (char)(r.nextInt(26) + 'a');        
        return res;
    }
}
