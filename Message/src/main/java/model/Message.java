package model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	
    private String text;

    private Timestamp pasted;

    private long ownerId;
    
    public Message() {
    	pasted = new Timestamp(System.currentTimeMillis());
    }
    
    public long getId() {
    	return id;
    }
    
    public String getText() {
    	return text;
    }
    
    public Timestamp getPasted() {
    	return pasted;
    }
    
    public long getOwnerId() {
    	return ownerId;
    }

    public void setText(String text) {
    	this.text = text;
    }
    
    public void setOwnerId(long ownerId) {
    	this.ownerId = ownerId;
    }
}
