package repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import model.Message;

@Component
public interface MessageRepository extends CrudRepository<Message,Long>{

	List <Message> findByOwnerId(long ownerId);
}
