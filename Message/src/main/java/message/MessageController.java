package message;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.Message;
import model.MessageListResponse;
import model.PostMessageRequest;
import model.PostMessageResponse;
import repository.MessageRepository;

@RestController
public class MessageController {

	@Autowired
	MessageRepository messageRepository;

    @RequestMapping("/getMessage/{id}")
    @ResponseBody
    public Message message(@PathVariable("id") long id) {
    	return messageRepository.findOne(id);
    }
    
    @RequestMapping("/getMessages/{ownerId}")
    @ResponseBody
    public MessageListResponse messages(@PathVariable("ownerId") long ownerId,
    		@RequestParam(value = "page", required=false) Long page,
            @RequestParam(value = "size", required=false) Long size) {
        if (page == null) page = 1L;
        if (size == null) size = 10L;
        if (page == 0 || size == 0)
            throw new IllegalArgumentException("Zero page number or size are not allowed.");
        List <Message> msgs = messageRepository.findByOwnerId(ownerId);
        Long from = page * size - size;
        long pageCount = msgs.size() / size + (msgs.size() % size > 0 ? 1 : 0);
        msgs =
        		msgs.stream()
                        .skip(from)
                        .limit(size)
                        .collect(Collectors.toList());
        return new MessageListResponse(size, page, pageCount, msgs);
    }
    
    @RequestMapping(value = "/postMessage", method = RequestMethod.POST)
    @ResponseBody
    public PostMessageResponse postMessage(@ModelAttribute PostMessageRequest params) {
    	PostMessageResponse res = new PostMessageResponse();
    	Message msg = new Message();
    	msg.setText(params.getMessage());
    	msg.setOwnerId(params.getOwnerId());
    	messageRepository.save(msg);
    	
    	res.setId(msg.getId());
    	res.setCode(true);
    	return res;
    }
}
